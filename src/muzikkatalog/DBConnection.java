/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package muzikkatalog;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author arvkok8
 */
public class DBConnection {
    private Connection dbConnection;
    
    public DBConnection() {
        
    }
    
    public boolean startConnection() {
        boolean success = true;
        
        try {
            dbConnection = DriverManager.getConnection("jdbc:sqlite:veritabani.sqlite");
        }
        catch (SQLException e) {
            System.err.println("Bağlantı kurulamadı: " + e);
            success = false;
        }
        
        return success;
    }
    
    public boolean writeSchemaIfEmpty() {
        String sqlCreateAlbums = "CREATE TABLE IF NOT EXISTS \"albumler\" (\n" +
            "	\"id\"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
            "	\"ad\"	TEXT NOT NULL,\n" +
            "	\"yayinTarihi\"	TEXT NOT NULL\n" +
            ");\n";
        String sqlCreateSongs = "CREATE TABLE IF NOT EXISTS \"sarkilar\" (\n" +
            "	\"id\"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
            "	\"ad\"	TEXT NOT NULL,\n" +
            "	\"sanatci\"	TEXT NOT NULL,\n" +
            "	\"album\"	INTEGER NOT NULL,\n" +
            "	\"albumSira\"	INTEGER,\n" +
            "	FOREIGN KEY(\"album\") REFERENCES \"albumler\"(\"id\") ON DELETE CASCADE\n" +
            ");"; //Netbeans <3

        
        try {
            PreparedStatement albumStatement = dbConnection.prepareStatement(sqlCreateAlbums);
            PreparedStatement songStatement = dbConnection.prepareStatement(sqlCreateSongs);
            
            albumStatement.executeUpdate();
            songStatement.executeUpdate();
        }
        catch (SQLException e) {
            System.err.println("Veritabanı şeması oluşturulamadı: " + e);
            return false;
        }
        
        return true;
    }
    
    public ResultSet queryAlbumByID(int id) {
        String sqlQuery = "SELECT * FROM albumler WHERE id = ?";
        ResultSet rs;
        
        try {
            PreparedStatement statement = dbConnection.prepareStatement(sqlQuery);
            
            statement.setInt(1, id);
            
            rs = statement.executeQuery();
        }
        catch (SQLException e) {
            System.err.println("Albüm sorgusu başarısız oldu: " + e);
            return null;
        }
        
        return rs;
    }
    
    //Albüm isminde ara
    public ResultSet queryAlbum(String queryString) {
        String sqlQuery = "SELECT id, ad, yayinTarihi FROM albumler WHERE ad LIKE ?";
        ResultSet rs;
        
        try {
            PreparedStatement statement = dbConnection.prepareStatement(sqlQuery);
            
            statement.setString(1, "%" + queryString + "%");
            
            rs = statement.executeQuery();
        }
        catch (SQLException e) {
            System.err.println("Albüm sorgusu başarısız oldu: " + e);
            return null;
        }
        
        return rs;
    }
    
    //Albüme ait şarkıları ara
    public ResultSet querySongsByAlbum(int albumID) {
        String sqlQuery = "SELECT * FROM sarkilar WHERE album = ?";
        ResultSet rs;
        
        try {
            PreparedStatement statement = dbConnection.prepareStatement(sqlQuery);
            
            statement.setInt(1, albumID); //Integer için uğraşmaya gerek var mı?
            
            rs = statement.executeQuery();
        }
        catch (SQLException e) {
            System.err.println("Albüme göre şarkı sorgusu başarısız oldu: " + e);
            return null;
        }
        
        return rs;
    }
    
    //Detaylı sonuç döndür
    public ResultSet queryFormated(String queryString, String section) {
        //String sqlQuery = "SELECT * FROM sarkilar WHERE ad LIKE ?";
        String sqlQuery
                = "SELECT sarkilar.ad AS `sarkiAd`,"
                + "sarkilar.sanatci AS `sarkiSanatci`,"
                + "albumler.ad AS `albumAd`,"
                + "albumler.yayinTarihi AS `albumTarih`,"
                + "sarkilar.id AS `sarkiID`,"
                + "albumler.id AS `albumID`"
                + " FROM sarkilar INNER JOIN albumler ON sarkilar.album = albumler.id"
                + " WHERE " + section + " LIKE ?"; //god bless sql
        ResultSet rs;
        
        try {
            PreparedStatement statement = dbConnection.prepareStatement(sqlQuery);
            
            //statement.setString(1, section);
            statement.setString(1, "%" + queryString + "%");
            
            rs = statement.executeQuery();
        }
        catch (SQLException e) {
            System.err.println("Formatlı sorgu başarısız oldu: " + e);
            return null;
        }
        
        return rs;
    }
    
    public ResultSet querySongs(String queryString) {
        return queryFormated(queryString, "sarkiAd");
    }
    
    public ResultSet queryArtists(String queryString) {
        return queryFormated(queryString, "sarkiSanatci");
    }
    
    //Fonksiyonu parçalamak gerekiyor
    public boolean pushAlbum(AlbumPOD album) {
        if( album.songs.length < 1 ) {
            System.err.println("Albüm boş olmamalı");
            return false;
        }
        
        StringBuilder sqlSongQuery = new StringBuilder();
        String sqlAlbumInsertQuery = "INSERT INTO albumler (ad, yayinTarihi) VALUES (?, ?);"; //Veritabanına yazılan albümün idsini getir
        String sqlAlbumResultQuery = "SELECT id FROM albumler WHERE rowid = last_insert_rowid()";
        ResultSet albumResult;
        PreparedStatement statement;
        int albumID;
        
        //Albümü kaydet ve IDyi al
        try {
            statement = dbConnection.prepareStatement(sqlAlbumInsertQuery);
            PreparedStatement idStatement = dbConnection.prepareStatement(sqlAlbumResultQuery);
            
            statement.setString(1, album.name);
            statement.setString(2, album.date);
            
            statement.executeUpdate();
            albumResult = idStatement.executeQuery();
            
            if( !albumResult.next() ) throw new SQLException("Oluşturulan albüm hakkında bilgi alınamadı");
            
            albumID = albumResult.getInt("id");
        }
        catch (SQLException e) {
            System.err.println(album.name + " için kayıt oluşturulamadı: " + e);
            return false;
        }
        
        //Şarkılar için sorguyu inşa et ve yolla
        sqlSongQuery.append("INSERT INTO sarkilar (ad, sanatci, album, albumSira) VALUES ");
        //Format stringini hazırla
        for(int i = 0; i < album.songs.length; i++) {
            if( i != 0 ) sqlSongQuery.append(", "); //kullanıcı tek seferde yüz bin şarkı eklemek istemediği sürece rahatız
            sqlSongQuery.append("(?, ?, ?, ?)");
        }
        
        //Formatın içini doldur
        try {
            statement = dbConnection.prepareStatement(sqlSongQuery.toString());
            
            for(int i = 0; i < album.songs.length; i++) {
                statement.setString(i*4 + 1, album.songs[i].name); //Şarkı adı
                statement.setString(i*4 + 2, album.songs[i].artist); //Sanatçı
                statement.setInt(i*4 + 3, albumID); //Bulunduğu album id numarası
                statement.setInt(i*4 + 4, i); //Albümdeki sırası
            }
            
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.err.println(album.name + " albümüne ait şarkıların kaydı başarısız oldu: " + e);
            System.out.println(album.name + " siliniyor");
            try {
                statement = dbConnection.prepareStatement("DELETE FROM albumler WHERE id = ?");
                
                statement.setInt(1, albumID);
                
                statement.executeUpdate();
            }
            catch (SQLException ex) {
                System.err.println("Albümün silinmesi başarısız oldu: " + ex); //Ama neden?
                return false;
            }
        }
        
        
        return true;
    }
    
    public boolean updateAlbumMeta(int id, AlbumPOD album) {
        PreparedStatement statement;
        String sqlUpdate = "UPDATE albumler SET ad=?, yayinTarihi=? WHERE id=?;";
        
        try {
            statement = dbConnection.prepareStatement(sqlUpdate);
            
            statement.setString(1, album.name);
            statement.setString(2, album.date);
            statement.setInt(3, id);
            
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.err.println("Albüm bilgilerinin değiştirilmesi başarısız oldu: " + e);
            return false;
        }
        
        return true;
    }
    
    public boolean deleteAlbum(int id) {
        String sqlDeleteQuery = "DELETE FROM albumler WHERE id = ?;";
        PreparedStatement statement;
        //boolean result = false;
        
        try {
            statement = dbConnection.prepareStatement(sqlDeleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.err.println(id + " numaralı albüm silinemedi: " + e);
            return false;
        }
        
        return true;
    }
    
    public boolean updateSong(String newName, String newArtist, int... sidList) {
        StringBuilder sqlUpdateQueryBuild = new StringBuilder("UPDATE sarkilar SET ");
        PreparedStatement statement;
        
        if( newName == null && newArtist == null ) {
            System.err.println("Yeni şarkı adı ve/veya yeni sanatçı adı verilmeli");
            return false;
        }
        
        if( newName != null ) {
            sqlUpdateQueryBuild.append("ad = ?");
        }
        if( newArtist != null ) {
            sqlUpdateQueryBuild.append(( newName != null ? ", " : "") + "sanatci = ?");
            //TODO: Bu kadar iğrençleşmeden oluştur sorguyu
        }
        
        sqlUpdateQueryBuild.append(" WHERE id IN (?");
        for(int i = 1; i < sidList.length; i++) {
            sqlUpdateQueryBuild.append(", ?");
        }
        sqlUpdateQueryBuild.append(")");
        
        try {
            statement = dbConnection.prepareStatement(sqlUpdateQueryBuild.toString());
            int offset = 2;
            
            if( newName != null ) {
                statement.setString(1, newName);
            }
            if( newArtist != null ) {
                if( newName != null ) {
                    statement.setString(2, newArtist);
                    offset++;
                }
                else statement.setString(1, newArtist);
            }
            
            //Idleri yaz
            for(int i = offset; i < sidList.length + offset; i++) {
                statement.setInt(i, sidList[i - offset]);
            }
            
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.err.println("Şarkıların güncellemesi başarısız oldu: " + e);
            return false;
        }
        
        return true;
    }
    
    public boolean updateSongArtist(String newArtist, int... sidList) {
        if( sidList.length < 1 ) return false;
        
        return true;
    }
    
    //TODO: cpypasta fonksiyon tanımları yerine sorguyu tek bir fonksiyonda oluştur
    public boolean deleteSong(int sid) {
        String sqlDeleteQuery = "DELETE FROM sarkilar WHERE id = ?;";
        PreparedStatement statement;
        //boolean result = false;
        
        try {
            statement = dbConnection.prepareStatement(sqlDeleteQuery);
            
            statement.setInt(1, sid);
            
            statement.executeUpdate();
        }
        catch (SQLException e) {
            System.err.println(sid + " numaralı albüm silinemedi: " + e);
            return false;
        }
        
        return true;
    }
    
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return dbConnection.prepareStatement(sql);
    }
}
