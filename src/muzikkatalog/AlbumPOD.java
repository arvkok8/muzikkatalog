/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package muzikkatalog;

/**
 *
 * @author arvkok8
 */
public class AlbumPOD { //"Plain Old Data"
    public int id;
    public SongPOD[] songs;
    public String name;
    public String date;
    
    public AlbumPOD(int albumID, String albumName, String publishDate, SongPOD... songlist) {
        songs = songlist.clone();
        id = albumID;
        date = publishDate;
        name = albumName;
    }
}
