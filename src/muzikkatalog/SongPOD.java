/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package muzikkatalog;

/**
 *
 * @author arvkok8
 */
public class SongPOD { //"Plain Old Data"
    public int id;
    public String name;
    public String artist;
    
    public SongPOD(int songID, String songName, String artistName) {
        id = songID;
        name = songName;
        artist = artistName;
    }
}
