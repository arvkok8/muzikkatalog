/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package muzikkatalog;

import java.awt.Image;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicListUI;

/**
 *
 * @author arvkok8
 */
public class MainWindow extends javax.swing.JFrame {
    
    private DBConnection dbConn;

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();
        
        //Albüm kapakları için klasör yoksa oluştur
        try {
            Files.createDirectories(Paths.get("albumkapak/"));
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Albüm kapakları için klasör oluşturulamadı: " + e, "Klasör oluşturulamadı", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        
        dbConn = new DBConnection();
        dbConn.startConnection();
        dbConn.writeSchemaIfEmpty(); //Eğer veritabanı silindiyse şema baştan oluşturulsun
        
        tableResults.getColumnModel().getColumn(3).setPreferredWidth(24);
        tableResults.getColumnModel().getColumn(3).setResizable(false);
        
        //Album ID
        tableResults.getColumnModel().getColumn(4).setMinWidth(32);
        tableResults.getColumnModel().getColumn(4).setPreferredWidth(48);
        tableResults.getColumnModel().getColumn(4).setMaxWidth(48);
        tableResults.getColumnModel().getColumn(4).setResizable(false);
        
        //Şarkı ID
        tableResults.getColumnModel().getColumn(5).setMinWidth(32);
        tableResults.getColumnModel().getColumn(5).setPreferredWidth(48);
        tableResults.getColumnModel().getColumn(5).setMaxWidth(48);
        tableResults.getColumnModel().getColumn(5).setResizable(false);
        
        //Seçim değişim eventini kendimiz kaydetmemiz gerekiyor çünkü java hala 1995de yaşıyor
        tableResults.getSelectionModel().addListSelectionListener(new Kanser());
    }
    
    private void fillTableFormated(ResultSet rs) {
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        model.setRowCount(0);
        
        try {
            while( rs.next() ) {
                model.addRow(new Object[]{
                    rs.getString("sarkiAd"),
                    rs.getString("sarkiSanatci"),
                    rs.getString("albumAd"),
                    rs.getString("albumTarih"),
                    Integer.toString(rs.getInt("sarkiID")),
                    Integer.toString(rs.getInt("albumID"))
                });
            }
        }
        catch (SQLException e) {
            System.err.println("Tablo doldurması başarısız oldu: " + e);
        }
    }
    
    private void searchInSongName(String queryString) {
        ResultSet songSet;
        
        songSet = dbConn.querySongs(queryString);
        if( songSet == null ) return;
        
        //HashMap<Integer, HashMap<String, String>> albumCache = new HashMap<>();
        
        fillTableFormated(songSet);
    }
    
    private void searchInArtistName(String queryString) {
        ResultSet artistSet;
        
        artistSet = dbConn.queryArtists(queryString);
        if( artistSet == null ) return;
        
        fillTableFormated(artistSet);
    }
    
    private void searchInAlbumName(String queryString) {
        ResultSet albumSet;
        
        albumSet = dbConn.queryAlbum(queryString);
        if( albumSet == null ) return; //loglamaya gerek yok queryAlbum fonksiyonu bizim yerimize yapıyor
        
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        model.setRowCount(0);
        
        try {
            while( albumSet.next()) {
                /*model.addRow( new Object[]{
                    "haha", "yes",
                    rs.getString("ad"),
                    rs.getString("yayinTarihi"),
                    rs.getString("id"),
                    "A1A1A1"
                });*/
                ResultSet songSet = dbConn.querySongsByAlbum(albumSet.getInt("id"));
                int i = 0;
                
                /*if( sarkiSet.next() == false ) {
                    model.addRow( new Object[]{
                        "BOŞ ALBÜM",
                        "BOŞ ALBÜM",
                        albumSet.getString("ad"),
                        albumSet.getString("yayinTarihi"),
                        albumSet.getInt("id"),
                        "------"
                    });
                } else sarkiSet.beforeFirst();*/ //"ResultSet is TYPE_FORWARD_ONLY" https://i.imgur.com/EGaWRC2.png
                
                while( songSet.next() ) {
                    i++;
                    model.addRow( new Object[]{
                        songSet.getString("ad"),
                        songSet.getString("sanatci"),
                        albumSet.getString("ad"),
                        albumSet.getString("yayinTarihi"),
                        Integer.toString(songSet.getInt("id")),
                        Integer.toString(albumSet.getInt("id"))
                    });
                }
                
                if( i == 0 ) {
                    model.addRow( new Object[]{
                        "BOŞ ALBÜM",
                        "BOŞ ALBÜM",
                        albumSet.getString("ad"),
                        albumSet.getString("yayinTarihi"),
                        "",
                        Integer.toString(albumSet.getInt("id"))
                    });
                }
            }
        } catch (SQLException ex) {
            System.err.println("ResultSet hatası: " + ex);
        }
    }
    
    private void removeRowsByAID(int aid) {
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        
        for(int i = 0; i < tableResults.getRowCount(); i++) {
            int id = Integer.parseInt((String)tableResults.getValueAt(i, 5)); //TODO: magic sayılarrrr
            
            if( id != aid ) continue;
            
            //Eğer albüm idsı istenilene eşitse albüm değişene kadar sil
            while( i < tableResults.getRowCount() && Integer.parseInt((String)tableResults.getValueAt(i, 5)) == aid ) {
                model.removeRow(i);
            }
            
            i = 0; //Çok verimli bir yöntem değil ama milyonlarca nesne yoksa işimizi görür
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonSearch = new javax.swing.JButton();
        comboSearchCat = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableResults = new javax.swing.JTable();
        textboxQuery = new javax.swing.JTextField();
        labelStatus = new javax.swing.JLabel();
        buttonAddAlbum = new javax.swing.JButton();
        buttonDeleteAlbum = new javax.swing.JButton();
        buttonModAlbum = new javax.swing.JButton();
        labelAlbumArt = new javax.swing.JLabel();
        buttonAddAlbumArt = new javax.swing.JButton();
        buttonDeleteSong = new javax.swing.JButton();
        buttonModSong = new javax.swing.JButton();
        buttonDeleteAlbumArt = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Müzik Katalog Yönetimi");
        setMaximumSize(new java.awt.Dimension(2048, 2147483647));
        setMinimumSize(new java.awt.Dimension(1075, 450));

        buttonSearch.setText("Ara");
        buttonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSearchActionPerformed(evt);
            }
        });

        comboSearchCat.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Şarkı Adı", "Albüm", "Sanatçı" }));

        tableResults.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Şarkı", "Sanatçı", "Albüm", "Y. Tarihi", "SID", "AID"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableResults.setToolTipText("");
        jScrollPane1.setViewportView(tableResults);

        textboxQuery.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                textboxQueryKeyPressed(evt);
            }
        });

        labelStatus.setText("    ");
        labelStatus.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        buttonAddAlbum.setText("Albüm Ekle");
        buttonAddAlbum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddAlbumActionPerformed(evt);
            }
        });

        buttonDeleteAlbum.setText("Albümü Sil");
        buttonDeleteAlbum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteAlbumActionPerformed(evt);
            }
        });

        buttonModAlbum.setText("Albümü Düzenle");
        buttonModAlbum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonModAlbumActionPerformed(evt);
            }
        });

        labelAlbumArt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelAlbumArt.setText("Kayıtlı bir resim yok");
        labelAlbumArt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        labelAlbumArt.setMaximumSize(new java.awt.Dimension(256, 256));
        labelAlbumArt.setMinimumSize(new java.awt.Dimension(256, 256));
        labelAlbumArt.setName(""); // NOI18N
        labelAlbumArt.setPreferredSize(new java.awt.Dimension(256, 256));

        buttonAddAlbumArt.setText("Resim Ekle/Değiştir");
        buttonAddAlbumArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddAlbumArtActionPerformed(evt);
            }
        });

        buttonDeleteSong.setText("Şarkıyı Sil");
        buttonDeleteSong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteSongActionPerformed(evt);
            }
        });

        buttonModSong.setText("Şarkıyı Düzenle");
        buttonModSong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonModSongActionPerformed(evt);
            }
        });

        buttonDeleteAlbumArt.setText("Resim Sil");
        buttonDeleteAlbumArt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteAlbumArtActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboSearchCat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textboxQuery))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 621, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(buttonAddAlbum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonDeleteAlbum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonModAlbum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonDeleteSong, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonModSong, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonAddAlbumArt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonDeleteAlbumArt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(labelAlbumArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(labelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(buttonSearch)
                            .addComponent(comboSearchCat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textboxQuery, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(labelStatus))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelAlbumArt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(buttonAddAlbum)
                                .addGap(18, 18, 18)
                                .addComponent(buttonDeleteAlbum)
                                .addGap(18, 18, 18)
                                .addComponent(buttonDeleteSong)
                                .addGap(18, 18, 18)
                                .addComponent(buttonModAlbum)
                                .addGap(18, 18, 18)
                                .addComponent(buttonModSong)
                                .addGap(18, 18, 18)
                                .addComponent(buttonAddAlbumArt)
                                .addGap(18, 18, 18)
                                .addComponent(buttonDeleteAlbumArt)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAddAlbumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddAlbumActionPerformed
        AlbumPOD album;
        NewAlbumDialog dialog = new NewAlbumDialog(this, true);
        dialog.setVisible(true); //Pencere kapanana kadar bloklanıyor
        
        if( !dialog.isConfirmed() ) {
            return;
        }
        
        album = dialog.getAlbum();
        
        if( dbConn.pushAlbum(album) ) {
            labelStatus.setText("Albüm ekleme başarılı");
        }
        else labelStatus.setText("Albüm ekleme başarısız");
    }//GEN-LAST:event_buttonAddAlbumActionPerformed

    private void buttonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSearchActionPerformed
        String queryString = textboxQuery.getText();
        
        switch( comboSearchCat.getSelectedIndex() ) {
            case 0: //Şarkı adına göre ara
                searchInSongName(queryString);
                break;
                
            case 1: //Albüm adına göre ara
                searchInAlbumName(queryString); //Tüm mantığı tek bir fonksiyona sıkıştırmak ne kadar mantıklı emin değilim
                break;
                
            case 2: //Sanatçı adına göre ara
                searchInArtistName(queryString);
                break;
        }
        
        labelStatus.setText("Buluan sonuç sayısı: " + tableResults.getRowCount());
    }//GEN-LAST:event_buttonSearchActionPerformed

    private void textboxQueryKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textboxQueryKeyPressed
        if( evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER ) {
            buttonSearchActionPerformed(null);
        }
    }//GEN-LAST:event_textboxQueryKeyPressed

    private void buttonDeleteAlbumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteAlbumActionPerformed
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        HashMap<Integer, String> albums = new HashMap();
        StringBuilder verifyString = new StringBuilder();
        int start = tableResults.getSelectedRow();
        int end = start + tableResults.getSelectedRowCount();
        
        if( start < 0 ) {
            labelStatus.setText("Albüm silmek için en az bir adet şarkı seçiniz");
            return;
        }
        
        verifyString.append("Aşşağıdaki albümler ve albümün içindeki bütün şarkıları silmek istediğinize emin misiniz?\n");
        
        for(int i = start; i < end; i++) {
            int id = Integer.parseInt((String)tableResults.getValueAt(i, 5));
            
            if( albums.containsKey(id) ) continue;
            
            albums.put(id, (String)tableResults.getValueAt(i, 2)); //TODO: magic sayılar olmadan yapmanın yolunu bul
            
            verifyString.append("\"" + albums.get(id) + "\" ");
        }
        
        if( albums.isEmpty() ) {
            labelStatus.setText("Silinecek albüm bulunamadı");
            return;
        }
        
        int result = JOptionPane.showConfirmDialog(null, verifyString, "Uyarı", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        
        if( result == JOptionPane.YES_OPTION ) {
            for(Map.Entry<Integer, String> e : albums.entrySet()) {
                if( dbConn.deleteAlbum(e.getKey()) ) {
                    //Tablodaki albümleri sil
                    removeRowsByAID(e.getKey());
                }
                else System.err.println(e.getValue() + " adlı albüm silinemedi");
            }
        }
        
        labelStatus.setText("Albümler başarıyla silindi");
    }//GEN-LAST:event_buttonDeleteAlbumActionPerformed

    private void buttonDeleteSongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteSongActionPerformed
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        int start = tableResults.getSelectedRow();
        int end = start + tableResults.getSelectedRowCount();
        
        if( start < 0 ) {
            labelStatus.setText("Silmek için en az bir şarkı seçin");
            return;
        }
        
        if( end - start > 1) {
            int result = JOptionPane.showConfirmDialog(null, 
                    "Birden fazla şarkı silmek istediğinze emin misiniz?",
                    "Uyarı",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE
            );

            if( result == JOptionPane.NO_OPTION ) {
                labelStatus.setText("");
                return;
            }
        }
        
        
        for(int i = start; i < end; i++) {
            String val = (String)model.getValueAt(start, 4); //magic sayılarrrr
            if( val.length() < 1 ) continue;
            int id = Integer.parseInt(val);
            
            if( dbConn.deleteSong(id) ) {
                model.removeRow(start);
            }
        }
        
        labelStatus.setText("Şarkı(lar) başarıyla silindi");
    }//GEN-LAST:event_buttonDeleteSongActionPerformed

    private void buttonModAlbumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonModAlbumActionPerformed
        int start = tableResults.getSelectedRow();
        if( start < 0 ) {
            labelStatus.setText("Değiştirmek istediğiniz albüme ait şarkıyı seçin");
            return;
        }
        
        if( tableResults.getSelectedRowCount() > 1 ) {
            labelStatus.setText("Sadece bir adet albüm/şarkı seçebilirsiniz");
            return;
        }
        
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        AlbumPOD selectedAlbum;
        AlbumPOD newAlbum;
        ModAlbumDialog dialog = new ModAlbumDialog(this, true);
        
        selectedAlbum = new AlbumPOD(
                Integer.parseInt((String)model.getValueAt(start, 5)),
                (String)model.getValueAt(start, 2),
                (String)model.getValueAt(start, 3),
                new SongPOD[0]
        );
        
        dialog.setAlbum(selectedAlbum.id, selectedAlbum.name, selectedAlbum.date);
        dialog.setVisible(true);
        
        if( !dialog.isConfirmed() ) {
            labelStatus.setText("Albüm düzenleme iptal edildi");
            return;
        }
        
        newAlbum = dialog.getAlbum();
        if( dbConn.updateAlbumMeta(newAlbum.id, newAlbum) ) {
            labelStatus.setText("Albüm bilgisi başarıyla değiştirildi");
        } else labelStatus.setText("Albüm bilgisi değiştirilemedi");
        
        //Tablodaki tüm albümlerin bilgilerini değiştir
    }//GEN-LAST:event_buttonModAlbumActionPerformed

    private void buttonModSongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonModSongActionPerformed
        int start = tableResults.getSelectedRow();
        int count = tableResults.getSelectedRowCount();
        if( start < 0 ) {
            labelStatus.setText("Değiştirmek istediğiniz albüme ait şarkıyı seçin");
            return;
        }
        
        ModSongDialog dialog = new ModSongDialog(this, true);
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        int[] songIDs = new int[count];
        int firstAlbumID = Integer.parseInt((String)model.getValueAt(start, 5));
        String artistName = (String)model.getValueAt(start, 1);
        String songName = (String)model.getValueAt(start, 0);
        SongPOD newSong;
        
        for(int i = 0; i < count; i++) {
            if( Integer.parseInt((String)model.getValueAt(start + i, 5)) != firstAlbumID ) {
                labelStatus.setText("Sadece aynı albümdeki şarkıları seçin");
                return;
            }
            
            songIDs[i] = Integer.parseInt((String)model.getValueAt(start + i, 4));
        }
        
        //if( count > 1 ) dialog.setSong(-1, null, artistName, true);
        //else dialog.setSong(songIDs[0], songName, artistName, false);
        dialog.setSong(songIDs[0], songName, artistName, count > 1);
        
        dialog.setVisible(true);
        
        if( !dialog.isConfirmed() ) {
            labelStatus.setText("İptal edildi");
            return;
        }
        
        newSong = dialog.getSong();
        
        if( !dbConn.updateSong( (count > 1 ? null : newSong.name), newSong.artist, songIDs) ) {
            labelStatus.setText("Seçilen şarkıların düzenlenmesi başarısız oldu");
        }
        
    }//GEN-LAST:event_buttonModSongActionPerformed

    private void buttonAddAlbumArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddAlbumArtActionPerformed
        int start = tableResults.getSelectedRow();
        if( start < 0 ) {
            labelStatus.setText("Albüm kapağı eklemek istediğiniz albüme ait şarkıyı seçin");
            return;
        }
        
        if( tableResults.getSelectedRowCount() > 1 ) {
            labelStatus.setText("Sadece bir adet albüm/şarkı seçebilirsiniz");
            return;
        }
        
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        String albumID = (String)model.getValueAt(start, 5);
        JFileChooser fc = new JFileChooser();
        int dialogReturn;
        
        fc.addChoosableFileFilter(new FileNameExtensionFilter(
                "Resimler: *.jpeg, *.jpg, *.png, *.bmp",
                "jpeg", "jpg", "png", "bmp"
        ));
        fc.setAcceptAllFileFilterUsed(false);
        fc.setCurrentDirectory(Paths.get(".").toFile());
        
        dialogReturn = fc.showOpenDialog(null);
        
        if( dialogReturn != JFileChooser.APPROVE_OPTION ) {
            labelStatus.setText("İşlem iptal edildi");
            return;
        }
        
        //System.out.println("File: " + fc.getSelectedFile().getPath());
        
        try {
            Files.copy(fc.getSelectedFile().toPath(), Paths.get("albumkapak/" + albumID), REPLACE_EXISTING); //ndn böylesin java
        }
        catch (IOException e) {
            labelStatus.setText("Albüm kapağı değiştirilemedi");
            System.err.println("Albüm kapağı kopyalanamadı: " + e);
            return;
        }
        
        labelStatus.setText("Albüm kapağı başarıyla değiştirildi");
    }//GEN-LAST:event_buttonAddAlbumArtActionPerformed

    private void buttonDeleteAlbumArtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteAlbumArtActionPerformed
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        int selection = tableResults.getSelectedRow();
        
        if( selection < 0 ) {
            labelStatus.setText("Kapağını silmek istediğiniz albüme ait bir şarkı seçin");
            return;
        }
        
        int albumID = Integer.parseInt((String)model.getValueAt(selection, 5));
        boolean result = false;
        
        try {
            result = Files.deleteIfExists(Paths.get("albumkapak/" + albumID));
        }
        catch (IOException e) {
            labelStatus.setText("Albüm kapağı silinirken hata oluştur");
            System.err.println("Albüm kapağı silinemedi: " + e);
            return;
        }
        
        if( result ) {
            labelStatus.setText("Albüm kapağı başarıyla silindi");
        }
        else labelStatus.setText("Albüm kapağı silinemedi");
    }//GEN-LAST:event_buttonDeleteAlbumArtActionPerformed

    private void tableResultsSelectionChanged() {
        DefaultTableModel model = (DefaultTableModel)tableResults.getModel();
        
        int selection = tableResults.getSelectedRow();
        if( selection < 0 ) {
            labelAlbumArt.setIcon(null);
            labelAlbumArt.setText("Kayıtlı bir resim yok");
            return;
        }
        int albumID = Integer.parseInt((String)model.getValueAt(selection, 5));
        
        ImageIcon icon = new ImageIcon("albumkapak/" + albumID);
        Image iconImg = icon.getImage();
        iconImg.flush();
        
        //Albüm kapağının boyutları 1:1 değil ise boyutları ayarla
        int w = iconImg.getWidth(null);
        int h = iconImg.getHeight(null);
        int max = Math.max(w, h);
        int labelBorder = labelAlbumArt.getSize().height;
        float r = (float)labelBorder/max;
        //float r = Math.min(, b)
        
        //icon = new ImageIcon(iconImg.getScaledInstance( (int)(w*r), (int)(h*r), Image.SCALE_SMOOTH));
        icon.setImage(iconImg.getScaledInstance( (int)(w*r), (int)(h*r), Image.SCALE_SMOOTH));
        labelAlbumArt.setIcon(icon);
        if( w < 0 || h < 0 ) labelAlbumArt.setText("Kayıtlı bir resim yok");
        else labelAlbumArt.setText("");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAddAlbum;
    private javax.swing.JButton buttonAddAlbumArt;
    private javax.swing.JButton buttonDeleteAlbum;
    private javax.swing.JButton buttonDeleteAlbumArt;
    private javax.swing.JButton buttonDeleteSong;
    private javax.swing.JButton buttonModAlbum;
    private javax.swing.JButton buttonModSong;
    private javax.swing.JButton buttonSearch;
    private javax.swing.JComboBox<String> comboSearchCat;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelAlbumArt;
    private javax.swing.JLabel labelStatus;
    private javax.swing.JTable tableResults;
    private javax.swing.JTextField textboxQuery;
    // End of variables declaration//GEN-END:variables

    private class Kanser implements ListSelectionListener {
        @Override
        public void valueChanged(ListSelectionEvent ev) {
            tableResultsSelectionChanged();
        }
    }
}
